const http = require('http');

//Creates a variable "port" to store the port number
const port = 4000;

//Create a variable "server" that stores the output of the "createServer" method
const server = http.createServer((req, res) => {

	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Hello from the other side!")
	} else if (req.url == '/home') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("This is the home page!")
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end("Page not available")
	}

});

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);