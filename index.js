//Use the "require" directive to load Node.js modules
/*
	"http module"
		lets Node.js transfer data using Hyper Text Transfer Protocol

		is a set of individual files that contains codes to create a component that helps establish data transfer between applications

	HTTP
		is a protocol that allows the fetching of resources such as HTML documents
*/

let http = require("http");

//The "http module" has a createServer() method that accepts a function as an argument and allows for a creation of server
http.createServer(function(request, response) {
	
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World!');

}).listen(4000);

//A port is a virtual point where network connections start and end
//The server will be assigned to port "4000" via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server

console.log("Server running at localhost:4000");